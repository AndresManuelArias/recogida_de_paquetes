## desarrollo
### `deno install -qAf --unstable https://deno.land/x/denon/denon.ts`
### `denon start`

## recargar la instalacion
### `deno install --allow-read --allow-run --allow-write -f --unstable --reload https://deno.land/x/denon/denon.ts`

# limpiar la chache
### `deno cache --reload app.ts`

# Arrancar mongo en linux
### `sudo systemctl start mongod`

# Arrancar mongo en app
### `deno run --allow-net --allow-read --allow-write --import-map=import_map.json app.ts`


# Compilados
###  `deno bundle --import-map=import_map.json app.ts  app.bundle.js`
###  `deno compile -A --unstable app.bundle.js`
###  `deno compile -A --unstable --import-map=import_map.json app.ts app.ts`
### `deno compile --target=x86_64-pc-windows-msvc --allow-run --allow-net --allow-read  app.bundle.js`

# Arrancar compilados
### `deno run --allow-net --allow-read --allow-write --import-map=import_map.json app.bundle.js`