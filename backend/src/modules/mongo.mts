import { Bson, MongoClient } from "mongo/mod.ts";
import {AppRecogidaSchema} from "../dto/index.ts";

const client = new MongoClient();

//Connecting to a Local Database
await client.connect("mongodb://localhost:27017");

const db = client.database("mintic");


export const appRecogidaSchema = db.collection<AppRecogidaSchema>("AppRecogida");
