import {
Router,
} from "oak/mod.ts";

import {RecogidaPaquetes} from '../controllers/index.ts';

const router = new Router();
router
.post("/actualizar-estado-recogida",RecogidaPaquetes.actualizarRecogida)
.post("/agregar-recogida",RecogidaPaquetes.agregarRecogida)
.post("/asignar-paquete-mensajero",RecogidaPaquetes.asignarPaqueteMensajero)
.post("/crear-usuario",RecogidaPaquetes.crearUsuario)
.post("/ingresar-usuario",RecogidaPaquetes.ingresarUsuario)
.get("/", (context) => {
    context.response.body = "Hello world!";
})


export default router;
