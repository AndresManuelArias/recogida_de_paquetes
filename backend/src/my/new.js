async function bodyGet1(body) {
    let objeto = {
    };
    console.log({
        body
    });
    if (body.type === "json") {
        objeto = await body.value;
    } else if (body.type === "form") {
        objeto = {
        };
        for (const [key, value] of (await body.value)){
            objeto[key] = value;
        }
    } else if (body.type === "form-data") {
        const formData = await body.value.read();
        objeto = formData.fields;
    }
    return objeto;
}
export { bodyGet1 as bodyGet };
