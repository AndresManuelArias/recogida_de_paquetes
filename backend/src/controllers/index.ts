import {bodyGet} from '../my/service.js';
import { Context,Status } from "oak/mod.ts"; 
import {ActualizarEstadoDeRecogida, AgregarRecogida, AsignarPaqueteMensajero, Usuario} from '../dto/index.ts';
import { appRecogidaSchema } from "../modules/mongo.mts";
import { create , verify} from "djwt/mod.ts";

const key = await crypto.subtle.generateKey(
    { name: "HMAC", hash: "SHA-512" },
    true,
    ["sign", "verify"],
  );

export class RecogidaPaquetes {
    static async actualizarRecogida(context:Context):Promise< void>{
        let actualizarEstadoDeRecogida:ActualizarEstadoDeRecogida = await bodyGet(context.request.body()) as ActualizarEstadoDeRecogida;
        console.log({actualizarEstadoDeRecogida});
        let insertId = await  appRecogidaSchema.insertOne({
            actualizarEstadoDeRecogida
        });
        console.log({insertId});
        console.log(insertId["toHexString"]());
        const rest:{actualizarEstadoDeRecogida:ActualizarEstadoDeRecogida} = await appRecogidaSchema.findOne({ _id: insertId }) as {actualizarEstadoDeRecogida:ActualizarEstadoDeRecogida};
        console.log({rest});        
        context.response.status = Status.Created;
        context.response.body = rest.actualizarEstadoDeRecogida;
    }
    static async agregarRecogida(context:Context):Promise< void>{
        let agregarRecogida:AgregarRecogida = await bodyGet(context.request.body()) as AgregarRecogida;
        console.log({agregarRecogida});
        let insertId = await  appRecogidaSchema.insertOne({
            agregarRecogida
        });
        console.log({insertId});
        console.log(insertId["toHexString"]());
        const rest:{agregarRecogida:AgregarRecogida} = await appRecogidaSchema.findOne({ _id: insertId }) as {agregarRecogida:AgregarRecogida};
        console.log({rest});        
        context.response.status = Status.Created;
        context.response.body = rest.agregarRecogida;
    }
    static async asignarPaqueteMensajero(context:Context):Promise< void>{
        let asignarPaqueteMensajero:AsignarPaqueteMensajero = await bodyGet(context.request.body()) as AsignarPaqueteMensajero;
        console.log({asignarPaqueteMensajero});
        let insertId = await  appRecogidaSchema.insertOne({
            asignarPaqueteMensajero
        });
        console.log({insertId});
        console.log(insertId["toHexString"]());
        const rest:{asignarPaqueteMensajero:AsignarPaqueteMensajero} = await appRecogidaSchema.findOne({ _id: insertId }) as {asignarPaqueteMensajero:AsignarPaqueteMensajero};
        console.log({rest});        
        context.response.status = Status.Created;
        context.response.body = rest.asignarPaqueteMensajero;
    }
    static async crearUsuario(context:Context):Promise< void>{
        let usuario:Usuario = await bodyGet(context.request.body()) as Usuario;
        console.log({usuario});
        let {contrasena} = usuario
        const jwt = await create({ alg: "HS512", typ: "JWT" },{contrasena}, key);
        usuario.contrasena = jwt;
        // This is AES-128-CBC. It works.
        let insertId = await  appRecogidaSchema.insertOne({
            usuario
        });
        console.log({insertId});
        console.log(insertId["toHexString"]());
        const rest:{usuario:Usuario} = await appRecogidaSchema.findOne({ _id: insertId }) as {usuario:Usuario};
        console.log({rest});
        const payload = await verify(rest.usuario.contrasena, key);
        console.log({payload});    
        rest.usuario.contrasena = payload.contrasena as string
        context.response.status = Status.Created;
        context.response.body = rest.usuario;
    }
    static async ingresarUsuario(context:Context):Promise< void>{
        let usuario:Usuario = await bodyGet(context.request.body()) as Usuario;
        console.log({usuario});
        let {contrasena} = usuario
        const jwt = await create({ alg: "HS512", typ: "JWT" },{contrasena}, key);
        // This is AES-128-CBC. It works.
        console.log({jwt});
        
        const rest:{usuario:Usuario} = await appRecogidaSchema.findOne({ 
            $and: [
              {"usuario.nickname":usuario.nickname},
              {"usuario.contrasena":jwt},
            ]
          }) as {usuario:Usuario};
        console.log({rest});
        console.log({Status});
        
        if(rest == undefined){
            context.response.status = Status.Unauthorized;
            context.response.body = {info:'usuario o contraseña incorrecta'}
        }else{
            let {nickname,permisos}= rest.usuario
            context.response.status = Status.Created;
            context.response.body = {nickname,permisos};
        }
    
    }
}