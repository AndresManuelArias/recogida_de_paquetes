export interface ActualizarEstadoDeRecogida{
    asignarEstadoDelEnvio:string,
    codigoDelPaquete:number,
}
export interface AgregarRecogida{
    fechaRecogida:Date,
    dimensiones:number,
    identificacionPersonal:string,
    direccion:string,
}
export interface AsignarPaqueteMensajero{
    usuariosDeMensajería:string,
    codigoDelPaquete:string,
}
export interface Usuario{
    nickname:string
    contrasena:string
    permisos:string
}

export interface AppRecogidaSchema {
    _id: { $oid: string };
    actualizarEstadoDeRecogida:ActualizarEstadoDeRecogida
    agregarRecogida:AgregarRecogida
    asignarPaqueteMensajero:AsignarPaqueteMensajero
    usuario:Usuario
}