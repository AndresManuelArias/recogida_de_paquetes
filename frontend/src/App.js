import React from "react";
import ActualizarEstadoRecogida from './views/ActualizarEstadoRecogida/ActualizarEstadoRecogida.js'
import AgregarRecogida from './views/AgregarRecogida/AgregarRecogida.js'
import AsignarPaqueteMensajero from './views/AsignarPaqueteMensajero/AsignarPaqueteMensajero.js'
import Loging from './views/Loging/Loging.js'
import Ingress from './views/Loging/Ingress.js'
import {
  BrowserRouter as Router,
  Routes,
  Route,
  useRoutes,
  NavLink,
} from "react-router-dom";
{/* <ActualizarEstadoRecogida/>,
<AgregarRecogida />,
<AsignarPaqueteMensajero />, */}
const Component1 = () => {
  return <h1>Component 1</h1>;
};

const Component2 = () => {
  return <h1>Component 2</h1>;
};
;
let permiso = sessionStorage.getItem('permiso');
const AppR = () => {
  let rutas = [
    {  path: "/actualizar-estado-recogida", element:permiso=='1'? <ActualizarEstadoRecogida/>:"" },
    {  path: "/agregar-recogida", element:permiso=='2'?  <AgregarRecogida/>:"" },
    {  path: "/asignar-paquete-mensajero", element: permiso=='3'?  <AsignarPaqueteMensajero/>:"" },
    {  path: "/crear-usuario", element: <Loging/> },
    {  path: "/ingresar-usuario", element: <Ingress/> },

    { path: "component2", element: <Component2 /> },
    // ...
  ]
  let routes = useRoutes(rutas);
  return routes;
};

const App = () => {
    let completed = false;
    let id = 0;
    let llenarLista = (n)=>{
        console.log(n)
        return ()=>{
             id = n
        }
    }
  return (
    <Router>
 <div className="App">
      <div class="d-flex" id="wrapper">

        <div class="border-end bg-white" id="sidebar-wrapper">
            <div class="sidebar-heading border-bottom bg-light">Start Bootstrap</div>
            <div class="list-group list-group-flush">
             
                <NavLink  to="/actualizar-estado-recogida"  className={isActive =>
                  "list-group-item list-group-item-action list-group-item-light p-3 " + (isActive.isActive ? "dropdown-item active" : "")
                 }>
                  Actualizar estado de recogida
                </NavLink>  
              
              
                <NavLink to="/agregar-recogida"  className={isActive =>
                    "list-group-item list-group-item-action list-group-item-light p-3 " + (isActive.isActive ? "dropdown-item active" : "")
                }>
                    Agregar recogida 
                    </NavLink>
                    <NavLink to="/asignar-paquete-mensajero"  className={isActive =>
                    "list-group-item list-group-item-action list-group-item-light p-3 " + (isActive.isActive ? "dropdown-item active" : "")
                }>
                   Asignar paquete a mensajero
                    </NavLink>
               
            </div>
        </div>
        <div id="page-content-wrapper">

            <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                <div class="container-fluid">
                    <button class="btn btn-primary" id="sidebarToggle">Toggle Menu</button>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ms-auto mt-2 mt-lg-0">
                            <li class="nav-item active"><a class="nav-link" href="#!">Home</a></li>
                            <li class="nav-item"><a class="nav-link" href="#!">Link</a></li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Login</a>
                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                <NavLink to="/crear-usuario"  className={isActive =>
                                      "list-group-item list-group-item-action list-group-item-light p-3 " + (isActive.isActive ? "dropdown-item active" : "")
                                  }>
                                    Crear usuario
                                  </NavLink>
                                  <NavLink to="/ingresar-usuario"  className={isActive =>
                                      "list-group-item list-group-item-action list-group-item-light p-3 " + (isActive.isActive ? "dropdown-item active" : "")
                                  }>
                                   Iniciar sesion
                                  </NavLink>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container-fluid">
               
            <AppR />

                

                
            </div>
        </div>
    </div>    
    </div>

      
    </Router>
  );
};

export default App;