import React from 'react';
import PropTypes from 'prop-types';
import styles from './AgregarRecogida.module.css';
import Button from '../../components/Button/Button.js';
import LabelImput from '../../components/LabelImput/LabelImput.js';
import action from '../../components/saveData/saveData.js';

const URL =  `${process.env.REACT_APP_URL}agregar-recogida`;
let handleSubmit = action('agregar-recogida',[
  'fechaRecogida',
  'dimension',
  'identificacion',
  'direccion',
])

const AgregarRecogida = () => (
  <div className={styles.AgregarRecogida}>
    <h1 class="mt-4">Agregar recogida</h1>
    <form method="POST"  action={URL} onSubmit={handleSubmit}  >
      <LabelImput name="fechaRecogida" text="Fecha recogida" type="date" />
      <LabelImput name="dimension" type="number" text="Dimensiones"/>
      <LabelImput name="identificacion" type="text" text="Identificación personal"/>
      <LabelImput name="direccion" type="text" text="Dirección"/>

      <Button texto="Registrar" />
    </form>
    <div id="rest">     </div>

  </div>
);

AgregarRecogida.propTypes = {};

AgregarRecogida.defaultProps = {};

export default AgregarRecogida;
