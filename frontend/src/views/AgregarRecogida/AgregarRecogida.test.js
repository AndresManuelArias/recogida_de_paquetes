import React from 'react';
import ReactDOM from 'react-dom';
import AgregarRecogida from './AgregarRecogida';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<AgregarRecogida />, div);
  ReactDOM.unmountComponentAtNode(div);
});