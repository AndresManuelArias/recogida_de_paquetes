import React from 'react';
import ReactDOM from 'react-dom';
import ActualizarEstadoRecogida from './ActualizarEstadoRecogida';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ActualizarEstadoRecogida />, div);
  ReactDOM.unmountComponentAtNode(div);
});