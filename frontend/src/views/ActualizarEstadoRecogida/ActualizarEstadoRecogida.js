import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import styles from './ActualizarEstadoRecogida.module.css';
import Button from '../../components/Button/Button.js';
import LabelImput from '../../components/LabelImput/LabelImput.js';
import action from '../../components/saveData/saveData.js';



const URL =  `${process.env.REACT_APP_URL}actualizar-estado-recogida`;
let handleSubmit = action('actualizar-estado-recogida',['estado' , 'codigo'])




const ActualizarEstadoRecogida = () => (
  <div className={styles.ActualizarEstadoRecogida}>
    <h1 class="mt-4">Actualizar estado de recogida</h1>
    <form method="POST" action={URL} onSubmit={handleSubmit}  >
      <LabelImput name="estado" text="Asignar estado del envio"/>
      <LabelImput name="codigo" type="number" text="Codigo del paquete"/>
      <Button texto="Actualizar paquete" />
    </form>
    <div id="rest">     </div>
  </div>
);

ActualizarEstadoRecogida.propTypes = {};

ActualizarEstadoRecogida.defaultProps = {};

export default ActualizarEstadoRecogida;
