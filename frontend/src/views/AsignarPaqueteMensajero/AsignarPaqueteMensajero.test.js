import React from 'react';
import ReactDOM from 'react-dom';
import AsignarPaqueteMensajero from './AsignarPaqueteMensajero';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<AsignarPaqueteMensajero />, div);
  ReactDOM.unmountComponentAtNode(div);
});