import React from 'react';
import PropTypes from 'prop-types';
import styles from './AsignarPaqueteMensajero.module.css';
import Button from '../../components/Button/Button.js';
import LabelImput from '../../components/LabelImput/LabelImput.js';
import action from '../../components/saveData/saveData.js';

const URL =  `${process.env.REACT_APP_URL}asignar-paquete-mensajero`;

let handleSubmit = action('asignar-paquete-mensajero',[
  'usuarioMensajeria',
  'codigoPaquete',
])

const AsignarPaqueteMensajero = () => (
  <div className={styles.AsignarPaqueteMensajero}>
    <h1 class="mt-4">Asignar paquete a mensajero</h1>

    <form method="POST" action={URL} onSubmit={handleSubmit} >
      <LabelImput name="usuarioMensajeria" text="Usuarios de mensajería" type="text" />
      <LabelImput name="codigoPaquete" text="Codigo del paquete" type="text" />
      
      <Button texto="Agregar paquete" />
    </form>
    <div id="rest">     </div>

  </div>
);

AsignarPaqueteMensajero.propTypes = {};

AsignarPaqueteMensajero.defaultProps = {};

export default AsignarPaqueteMensajero;
