import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Button from '../../components/Button/Button.js';
import LabelImput from '../../components/LabelImput/LabelImput.js';
import Select from '../../components/Select/Select.js';
import action from '../../components/saveData/saveData.js';



const URL =  `${process.env.REACT_APP_URL}ingresar-usuario`;
let handleSubmit = action('ingresar-usuario',['nickname',
  'contrasena'],(rest)=>{
    debugger
    if(rest.permisos){
      sessionStorage.setItem('permiso', rest.permisos);
      window.location.reload();
    }
  
  })




const Ingress = () => (
  <div >
    <h1 class="mt-4">Ingresar</h1>
    <form method="POST" action={URL} onSubmit={handleSubmit}  >
      <LabelImput name="nickname" text="Nombre de usuario"/>
      <LabelImput name="contrasena" type="password" text="Contraseña"/>
      <Button texto="Ingresar Usuario" />
    </form>
    <div id="rest">     </div>
  </div>
);

Ingress.propTypes = {};

Ingress.defaultProps = {};

export default Ingress;
