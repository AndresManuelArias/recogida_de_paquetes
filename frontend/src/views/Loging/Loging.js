import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Button from '../../components/Button/Button.js';
import LabelImput from '../../components/LabelImput/LabelImput.js';
import Select from '../../components/Select/Select.js';
import action from '../../components/saveData/saveData.js';



const URL =  `${process.env.REACT_APP_URL}crear-usuario`;
let handleSubmit = action('crear-usuario',['nickname',
  'contrasena',
  'permisos'])




const Loging = () => (
  <div >
    <h1 class="mt-4">Crear usuario</h1>
    <form method="POST" action={URL} onSubmit={handleSubmit}  >
      <LabelImput name="nickname" text="Nombre de usuario"/>
      <LabelImput name="contrasena" type="password" text="Contraseña"/>
      <Select name="permisos" text="Seleccione el permiso"/>
      <Button texto="Crear Usuario" />
    </form>
    <div id="rest">     </div>
  </div>
);

Loging.propTypes = {};

Loging.defaultProps = {};

export default Loging;
