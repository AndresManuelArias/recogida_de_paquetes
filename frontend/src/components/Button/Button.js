import React from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.css';

const Button = ({texto}) => (
  <div className={styles.Button}>
    <div className="d-grid gap-2 col-6 mx-auto">
      <button className="btn btn-primary" type="submit">{texto}</button>
    </div>
  </div>
);

Button.propTypes = {};

Button.defaultProps = {};

export default Button;
