import React from 'react';
import PropTypes from 'prop-types';
import styles from './LabelImput.module.css';

const LabelImput = ({name="",text="",type="text"}) => (
  <div className={styles.LabelImput}>
    <div className="mb-3 d-grid gap-2 col-6 mx-auto">
      <label for={name} className="form-label">{text}</label>
      <input type={type} name={name} className="form-control" id={name} />
    </div>
  </div>
);

LabelImput.propTypes = {};

LabelImput.defaultProps = {};

export default LabelImput;
