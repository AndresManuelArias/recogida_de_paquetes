import React from 'react';
import ReactDOM from 'react-dom';
import LabelImput from './LabelImput';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<LabelImput />, div);
  ReactDOM.unmountComponentAtNode(div);
});