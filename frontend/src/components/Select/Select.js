import React from 'react';
import PropTypes from 'prop-types';

const Select = ({name="",text=""}) => (
  <div >

    
    <div className="mb-3 d-grid gap-2 col-6 mx-auto">

    
      <label for={name} className="form-label">{text}</label>
      <select  name={name}  className="mb-3 d-grid gap-2 col-6 mx-auto" id={name}>
        <option selected>Choose...</option>
        <option value="1">cliente</option>
        <option value="2">Mensajero</option>
        <option value="3">administrador</option>
      </select>
    </div>
  </div>
);

Select.propTypes = {};

Select.defaultProps = {};

export default Select;
