import React from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.css';
import ReactDOM from 'react-dom';



export default function action(path,properties,actionRest){
  const URL =  `${process.env.REACT_APP_URL}${path}`;
  let status= 200;
  return (e)  => {
    e.preventDefault();
    console.log('You clicked submit.');
    const body = {};
    properties.forEach(key => {
      let value =  document.getElementById(key).value;
      console.log({value});
      body[key]= value;
    });
    console.log({body});
    fetch(URL, {
      headers: {
           'Content-Type': 'application/json',
         },
       method: 'POST',
       body:JSON.stringify( body)
     
    }).then(res => { 
      console.log({res})
      status = res.status;
      return res.json()})
       .catch(error => console.error('Error:', error))
       .then(response =>{
          console.log('Success:', {response});
       const element = (
        <ul className={status== 401 ? 'alert alert-danger' : "alert alert-success"} >
          {Object.entries(response).map((data,index) =>
          
          {
            const regexp = /[A-Z]/g;
            debugger
            const array = [...data[0].matchAll(regexp)];
            let propieda = data[0].split(/[A-Z]/).map((p,i)=> `${p} ${array[i]?array[i]:''}`  ).join("").toLowerCase() 
            console.log({propieda})
            return <li key={index} > {propieda} : {data[1]} </li>
          }
        
          )}
        </ul>
      );
      ReactDOM.render(
        element,
        document.getElementById('rest')
      );
      debugger
      if(actionRest !==undefined){
        actionRest(response)
      }
      });
    return false;
  }
} 





